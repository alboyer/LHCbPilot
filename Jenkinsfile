#!/usr/bin/env groovy

/*
(c) Copyright 2019 CERN for the benefit of the LHCb Collaboration

This software is distributed under the terms of the GNU General Public
Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE"

In applying this licence, CERN does not waive the privileges and immunities
granted to it by virtue of its status as an Intergovernmental Organization
or submit itself to any jurisdiction.
*/


properties([parameters([string(name: 'projectVersion', defaultValue: 'v10r3-pre1', description: 'The LHCbDIRAC version to install'),
                        string(name: 'Pilot_repo', defaultValue: 'DIRACGrid', description: 'The Pilot repo'),
                        string(name: 'Pilot_branch', defaultValue: 'devel', description: 'The Pilot branch'),
                        string(name: 'LHCbPilot_repo', defaultValue: 'lhcb-dirac', description: 'The LHCbPilot repo'),
                        string(name: 'LHCbPilot_branch', defaultValue: 'devel', description: 'The LHCbPilot branch'),
                        string(name: 'DIRAC_test_repo', defaultValue: 'DIRACGrid', description: 'The DIRAC repo to use for getting the test code'),
                        string(name: 'DIRAC_test_branch', defaultValue: 'integration', description: 'The DIRAC branch to use for getting the test code'),
                        string(name: 'LHCbDIRAC_test_repo', defaultValue: 'lhcb-dirac', description: 'The LHCbDIRAC repo to use for getting the test code'),
                        string(name: 'LHCbDIRAC_test_branch', defaultValue: 'devel', description: 'The LHCbDIRAC branch to use for getting the test code'),
                        string(name: 'JENKINS_CE', defaultValue: 'jenkins.cern.ch', description: 'The CE definition to use (of DIRAC.Jenkins.ch)'),
                        string(name: 'modules', defaultValue: '', description: 'to override what is installed, e.g. with https://github.com/$DIRAC_test_repo/DIRAC.git:::DIRAC:::$DIRAC_test_branch,https://gitlab.cern.ch/$LHCbDIRAC_test_repo/LHCbDIRAC.git:::LHCbDIRAC:::$LHCbDIRAC_test_branch'),
                        string(name: 'pilot_options', defaultValue: '', description: 'any pilot option, e.g. -o diracInstallOnly or --pythonVersion=3')
                       ])])


node('lhcbci-cernvm4-03') {
    // Clean workspace before doing anything
    deleteDir()

    withEnv([
        "DIRACSETUP=LHCb-Certification",
        "CSURL=dips://lhcb-cert-conf-dirac.cern.ch:9135/Configuration/Server",
        "VO=LHCb",
        "PILOTCFG=pilot.cfg",
        "DIRACSE=CERN-SWTEST",
        "JENKINS_QUEUE=jenkins-queue_not_important",
        "JENKINS_SITE=DIRAC.Jenkins.ch",
        "DIRACUSERDN=/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=cburr/CN=761704/CN=Chris Burr",
        "DIRACUSERROLE=lhcb_prmgr"]){

        stage('GET') {

            echo "Here getting the code"

            dir("$WORKSPACE/TestCode"){
                sh """
                    git clone https://github.com/${params.Pilot_repo}/Pilot.git
                    cd Pilot
                    git checkout ${params.Pilot_branch}
                    cd ..
                """
                sh """
                    git clone https://github.com/${params.DIRAC_test_repo}/DIRAC.git
                    cd DIRAC
                    git checkout ${params.DIRAC_test_branch}
                    cd ..
                """

                sh """
                    git clone https://gitlab.cern.ch/${params.LHCbPilot_repo}/LHCbPilot.git
                    cd LHCbPilot
                    git checkout ${params.LHCbPilot_branch}
                    cd ..
                """
                sh """
                    git clone https://gitlab.cern.ch/${params.LHCbDIRAC_test_repo}/LHCbDIRAC.git
                    cd LHCbDIRAC
                    git checkout ${params.LHCbDIRAC_test_branch}
                    cd ..
                """

                echo "Got the test code"
            }
            stash includes: 'TestCode/**', name: 'testcode'
        }
        stage('SourceAndInstall') {

            echo "Sourcing and installing"
            sh """
                source $WORKSPACE/TestCode/Pilot/tests/CI/pilot_ci.sh
                fullPilot
                # Add a file in diracos/var/singularity/mnt/session/ as Jenkins doesn't
                # stash empty directories and Singularity requires this folder
                if [ -d "PilotInstallDIR/diracos/var/singularity/" ]; then
                  touch PilotInstallDIR/diracos/var/singularity/mnt/session/.keep-dir
                fi
            """
            echo "**** Pilot INSTALLATION DONE ****"

            stash includes: 'PilotInstallDIR/**', name: 'installation'

        }
        stage('Test') {
            echo "Starting the tests"

            parallel(

                "Core" : {
                    node('lhcbci-cernvm4-02') {

                        cleanWs()

                        unstash "installation"
                        unstash "testcode"

                        try {
                            dir(env.WORKSPACE+"/PilotInstallDIR"){
                                sh '''
                                    bash -c "source bashrc;\
                                    source diracos/diracosrc;\
                                    source \$WORKSPACE/TestCode/Pilot/tests/CI/pilot_ci.sh;\
                                    downloadProxy;\
                                    python \$WORKSPACE/TestCode/LHCbDIRAC/tests/Integration/Core/Test_RunApplication.py pilot.cfg -o /DIRAC/Security/UseServerCertificate=no -ddd"
                                '''
                            }
                        } catch (e) {
                            // if any exception occurs, mark the build as failed
                            currentBuild.result = 'FAILURE'
                            throw e
                        } finally {
                            // perform workspace cleanup only if the build have passed
                            // if the build has failed, the workspace will be kept
                            cleanWs cleanWhenFailure: false
                        }
                    }
                },

                "MC" : {
                    node('lhcbci-cernvm4-02') {

                        cleanWs()

                        unstash "installation"
                        unstash "testcode"

                        try {
                            dir(env.WORKSPACE+"/PilotInstallDIR"){
                                sh '''
                                    bash -c "source bashrc;\
                                    source diracos/diracosrc;\
                                    source \$WORKSPACE/TestCode/Pilot/tests/CI/pilot_ci.sh;\
                                    downloadProxy;\
                                    printenv;\
                                    python \$WORKSPACE/TestCode/LHCbDIRAC/tests/Workflow/Integration/Test_ProductionJobs_MC.py pilot.cfg -o /DIRAC/Security/UseServerCertificate=no -ddd"
                                '''
                            }
                        } catch (e) {
                            // if any exception occurs, mark the build as failed
                            currentBuild.result = 'FAILURE'
                            throw e
                        } finally {
                            // perform workspace cleanup only if the build have passed
                            // if the build has failed, the workspace will be kept
                            cleanWs cleanWhenFailure: false
                        }
                    }
                },


                "UserJobs" : {
                    node('lhcbci-cernvm4-02') {

                        cleanWs()

                        unstash "installation"
                        unstash "testcode"

                        try {
                            dir(env.WORKSPACE+"/PilotInstallDIR"){
                                sh '''
                                    bash -c "source bashrc;\
                                    source diracos/diracosrc;\
                                    source \$WORKSPACE/TestCode/Pilot/tests/CI/pilot_ci.sh;\
                                    downloadProxy;\
                                    python \$WORKSPACE/TestCode/LHCbDIRAC/tests/Workflow/Integration/Test_UserJobs.py pilot.cfg -o /DIRAC/Security/UseServerCertificate=no -ddd"
                                '''
                            }
                        } catch (e) {
                            // if any exception occurs, mark the build as failed
                            currentBuild.result = 'FAILURE'
                            throw e
                        } finally {
                            // perform workspace cleanup only if the build have passed
                            // if the build has failed, the workspace will be kept
                            cleanWs cleanWhenFailure: false
                        }
                    }
                },

                "RecoStripping" : {
                    node('lhcbci-cernvm4-02') {

                        cleanWs()

                        unstash "installation"
                        unstash "testcode"

                        try {
                            dir(env.WORKSPACE+"/PilotInstallDIR"){
                                sh '''
                                    bash -c "source bashrc;\
                                    source diracos/diracosrc;\
                                    source \$WORKSPACE/TestCode/Pilot/tests/CI/pilot_ci.sh;\
                                    downloadProxy;\
                                    python \$WORKSPACE/TestCode/LHCbDIRAC/tests/Workflow/Integration/Test_ProductionJobs_RecoStripping.py pilot.cfg -o /DIRAC/Security/UseServerCertificate=no -ddd"
                                '''
                            }
                        } catch (e) {
                            // if any exception occurs, mark the build as failed
                            currentBuild.result = 'FAILURE'
                            throw e
                        } finally {
                            // perform workspace cleanup only if the build have passed
                            // if the build has failed, the workspace will be kept
                            cleanWs cleanWhenFailure: false
                        }
                    }
                },

                "Merge" : {
                    node('lhcbci-cernvm4-02') {

                        cleanWs()

                        unstash "installation"
                        unstash "testcode"

                        try {
                            dir(env.WORKSPACE+"/PilotInstallDIR"){
                                sh '''
                                    bash -c "source bashrc;\
                                    source diracos/diracosrc;\
                                    source \$WORKSPACE/TestCode/Pilot/tests/CI/pilot_ci.sh;\
                                    downloadProxy;\
                                    printenv;\
                                    echo $PWD;\
                                    python \$WORKSPACE/TestCode/LHCbDIRAC/tests/Workflow/Integration/Test_ProductionJobs_Merge.py pilot.cfg -o /DIRAC/Security/UseServerCertificate=no -ddd"
                                '''
                            }
                        } catch (e) {
                            // if any exception occurs, mark the build as failed
                            currentBuild.result = 'FAILURE'
                            throw e
                        } finally {
                            // perform workspace cleanup only if the build have passed
                            // if the build has failed, the workspace will be kept
                            cleanWs cleanWhenFailure: false
                        }
                    }
                },

                "Regression_User" : {
                    node('lhcbci-cernvm4-01') {

                        cleanWs()

                        unstash "installation"
                        unstash "testcode"

                        try {
                            dir(env.WORKSPACE+"/PilotInstallDIR"){
                                sh '''
                                    bash -c "source bashrc;\
                                    source diracos/diracosrc;\
                                    source \$WORKSPACE/TestCode/Pilot/tests/CI/pilot_ci.sh;\
                                    downloadProxy;\
                                    python \$WORKSPACE/TestCode/LHCbDIRAC/tests/Workflow/Regression/Test_RegressionUserJobs.py pilot.cfg -o /DIRAC/Security/UseServerCertificate=no -ddd"
                                '''
                            }
                        } catch (e) {
                            // if any exception occurs, mark the build as failed
                            currentBuild.result = 'FAILURE'
                            throw e
                        } finally {
                            // perform workspace cleanup only if the build have passed
                            // if the build has failed, the workspace will be kept
                            cleanWs cleanWhenFailure: false
                        }
                    }
                },

                "Regression_MC" : {
                    node('lhcbci-cernvm4-01') {

                        cleanWs()

                        unstash "installation"
                        unstash "testcode"

                        try {
                            dir(env.WORKSPACE+"/PilotInstallDIR"){
                                sh '''
                                    bash -c "source bashrc;\
                                    source diracos/diracosrc;\
                                    source \$WORKSPACE/TestCode/Pilot/tests/CI/pilot_ci.sh;\
                                    downloadProxy;\
                                    python \$WORKSPACE/TestCode/LHCbDIRAC/tests/Workflow/Regression/Test_RegressionProductionJobs_MC.py pilot.cfg -o /DIRAC/Security/UseServerCertificate=no -ddd"
                                '''
                            }
                        } catch (e) {
                            // if any exception occurs, mark the build as failed
                            currentBuild.result = 'FAILURE'
                            throw e
                        } finally {
                            // perform workspace cleanup only if the build have passed
                            // if the build has failed, the workspace will be kept
                            cleanWs cleanWhenFailure: false
                        }
                    }
                },

                "Regression_MCReco" : {
                    node('lhcbci-cernvm4-01') {

                        cleanWs()

                        unstash "installation"
                        unstash "testcode"

                        try {
                            dir(env.WORKSPACE+"/PilotInstallDIR"){
                                sh '''
                                    bash -c "source bashrc;\
                                    source diracos/diracosrc;\
                                    source \$WORKSPACE/TestCode/Pilot/tests/CI/pilot_ci.sh;\
                                    downloadProxy;\
                                    python \$WORKSPACE/TestCode/LHCbDIRAC/tests/Workflow/Regression/Test_RegressionProductionJobs_MCReco.py pilot.cfg -o /DIRAC/Security/UseServerCertificate=no -ddd"
                                '''
                            }
                        } catch (e) {
                            // if any exception occurs, mark the build as failed
                            currentBuild.result = 'FAILURE'
                            throw e
                        } finally {
                            // perform workspace cleanup only if the build have passed
                            // if the build has failed, the workspace will be kept
                            cleanWs cleanWhenFailure: false
                        }
                    }
                },

                "Regression_Merge" : {
                    node('lhcbci-cernvm4-01') {

                        cleanWs()

                        unstash "installation"
                        unstash "testcode"

                        try {
                            dir(env.WORKSPACE+"/PilotInstallDIR"){
                                sh '''
                                    bash -c "source bashrc;\
                                    source diracos/diracosrc;\
                                    source \$WORKSPACE/TestCode/Pilot/tests/CI/pilot_ci.sh;\
                                    downloadProxy;\
                                    python \$WORKSPACE/TestCode/LHCbDIRAC/tests/Workflow/Regression/Test_RegressionProductionJobs_Merge.py pilot.cfg -o /DIRAC/Security/UseServerCertificate=no -ddd"
                                '''
                            }
                        } catch (e) {
                            // if any exception occurs, mark the build as failed
                            currentBuild.result = 'FAILURE'
                            throw e
                        } finally {
                            // perform workspace cleanup only if the build have passed
                            // if the build has failed, the workspace will be kept
                            cleanWs cleanWhenFailure: false
                        }
                    }
                },

                "Regression_RecoStripping" : {
                    node('lhcbci-cernvm4-01') {

                        cleanWs()

                        unstash "installation"
                        unstash "testcode"

                        try {
                            dir(env.WORKSPACE+"/PilotInstallDIR"){
                                sh '''
                                    bash -c "source bashrc;\
                                    source diracos/diracosrc;\
                                    source \$WORKSPACE/TestCode/Pilot/tests/CI/pilot_ci.sh;\
                                    downloadProxy;\
                                    python \$WORKSPACE/TestCode/LHCbDIRAC/tests/Workflow/Regression/Test_RegressionProductionJobs_RecoStripping.py pilot.cfg -o /DIRAC/Security/UseServerCertificate=no -ddd"
                                '''
                            }
                        } catch (e) {
                            // if any exception occurs, mark the build as failed
                            currentBuild.result = 'FAILURE'
                            throw e
                        } finally {
                            // perform workspace cleanup only if the build have passed
                            // if the build has failed, the workspace will be kept
                            cleanWs cleanWhenFailure: false
                        }
                    }
                }
            )
        }
    }
}
